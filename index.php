<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Tanzania Free Living Generation</title>

    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/style.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<nav id="nav-menu" class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">TZFLG</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav">
                <li><a href="#">About</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div>
</nav>

<div class="jumbotron">
    <div class="container">
        <div class="row text-center">
            <h2>Tanzania Free Living Generation</h2>
        </div>
    </div>
</div>

<section class="container">
    <h2 class="text-center">Services</h2>
    <div class="row text-center services">
        <div class="col-sm-3">
            <div class="thumbnail">
                <img src="http://lorempixel.com/400/200/food" alt="">
                <div class="caption">
                    <h3>People</h3>
                    <p>
                        Construct Chiba disposable cartel vehicle order-flow sub-orbital DIY augmented reality
                        crypto-BASE jump assault lights skyscraper.
                    </p>
                    <p>
                        <a href="#" class="btn btn-primary">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail">
                <img src="http://lorempixel.com/400/200/nature" alt="">
                <div class="caption">
                    <h3>Health</h3>
                    <p>
                        Tiger-team cyber-3D-printed rebar rifle plastic convenience store assault crypto-sensory vehicle
                        post-franchise.
                    </p>
                    <p>
                        <a href="#" class="btn btn-primary">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail">
                <img src="http://lorempixel.com/400/200/abstract" alt="">
                <div class="caption">
                    <h3>Children</h3>
                    <p>
                        Chrome concrete media rebar otaku jeans pen Chiba paranoid. Tower bomb city futurity assault
                        euro-pop apophenia fetishism.
                    </p>
                    <p>
                        <a href="#" class="btn btn-primary">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="thumbnail">
                <img src="http://lorempixel.com/400/200/technics" alt="">
                <div class="caption">
                    <h3>Education</h3>
                    <p>
                        Dolphin urban girl beef noodles city carbon corrupted camera A.I. systema smart-Kowloon-space
                        face forwards faded footage.
                    </p>
                    <p>
                        <a href="#" class="btn btn-primary">Learn more</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container-fluid text-center call-for-action">
    <p class="lead">
        Face forwards geodesic drone corporation uplink engine savant tiger-team sprawl footage beef noodles hotdog. <a
            href="#" class="btn btn-primary">Get Started</a>
    </p>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <h3 class="text-center">About</h3>
                <p class="text-justify">
                    Rain savant man camera gang smart-Shibuya. Cyber-San Francisco kanji semiotics sign realism towards
                    knife car range-rover boat Legba shanty town spook otaku. Narrative weathered tanto-ware drone
                    skyscraper papier-mache euro-pop cardboard.
                </p>
            </div>
            <div class="col-sm-4 text-center">
                <h3><i class="fa fa-envelope"></i> Newsletter</h3>
                <form>
                    <div class="form-group">
                        <input type="text" name="email" id="email" class="form-control" placeholder="Email Address">
                    </div>
                    <button type="submit" class="btn btn-warning btn-block">Subscribe</button>
                </form>
            </div>
            <div class="col-sm-4 text-center">
                <h3>Social</h3>
                <span><a href="#"><i class="fa fa-facebook-official btn-social"></i></a></span>
                <span><a href="#"><i class="fa fa-twitter btn-social"></i></a></span>
                <span><a href="#"><i class="fa fa-instagram btn-social"></i></a></span>
                <span><a href="#"><i class="fa fa-google-plus btn-social"></i></a></span>
            </div>
        </div>
    </div>
</footer>
<section class="text-center footer-bottom">
    TZFLG &copy; <?= date('Y') ?>. All Rights Reserved | Design by <a href="https://josephmtinangi.github.io"
                                                                      target="_blank">Joseph Mtinangi</a>
</section>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>
</html>
